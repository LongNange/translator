import EN as ENG
import DE as DEU
import FR as FRP
import Dictionary as Dict

D = Dict.Dictionary()


class Translator:
    def __init__(self):
        self.EN = ENG.English()
        self.DE = DEU.DE()
        self.FR = FRP.France()

    def set_text(self, text):
        self.DE.set_text(text)
        self.DE.parse()

    def get_text_DE(self):
        return self.DE.get_text()

    def get_text_EN(self):
        return self.EN.get_text()

    def get_text_FR(self):
        return self.FR.get_text()

    def get_color_text_EN(self):
        return self.EN.get_color_text()

    def get_color_text_FR(self):
        return self.FR.get_color_text()

    def translateDEtoEN(self):
        self.EN.A_Words.clear()
        self.create1()

        words = []
        for word in self.EN.A_Words:
            words.append(word.Word)

        text_EN = ' '.join(words)

        self.EN.set_text(text_EN)


    def create1(self):
        pass


    def translateENtoFR(self):
        self.FR.A_Words.clear()
        self.create2()

        words = []
        for word in self.FR.A_Words:
            words.append(word.Word)

        text_FR = ' '.join(words)

        self.FR.set_text(text_FR)


    def create2(self):
        pass

