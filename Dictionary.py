class Dictionary:
    def __init__(self):
        self.DE_Pronoun = []
        self.DE_Noun = []
        self.DE_Preposition = []

        self.EN_Pronoun = []
        self.EN_Noun = []
        self.EN_Preposition = []

        self.FR_Pronoun = []
        self.FR_Noun = []
        self.FR_Verbs = []
        self.FR_Preposition = []

        self.filling()

    def add_DE_Pronoun_Word(self, word):
        self.DE_Pronoun.append(word)

    def add_EN_Pronoun_Word(self, word):
        self.EN_Pronoun.append(word)

    def add_FR_Pronoun_Word(self, word):
        self.FR_Pronoun.append(word)

    def add_DE_Noun_Word(self, word, state):
        self.DE_Noun.append([word, state])

    def add_EN_Noun_Word(self, word, state):
        self.EN_Noun.append([word, state])

    def add_FR_Noun_Word(self, word, state):
        self.FR_Noun.append([word, state])

    def add_FR_Verbs_Word(self, word):
        self.FR_Verbs.append(word)

    def add_DE_Preposition_Word(self, word):
        self.DE_Preposition.append(word)

    def add_EN_Preposition_Word(self, word):
        self.EN_Preposition.append(word)

    def add_FR_Preposition_Word(self, word):
        self.FR_Preposition.append(word)

    def get_DE_Preposition(self, index):
        return self.DE_Preposition[index]

    def get_EN_Preposition(self, index):
        return self.EN_Preposition[index]

    def get_FR_Preposition(self, index):
        return self.FR_Preposition[index]

    def get_FR_Verb(self, index):
        return self.FR_Verbs[index]

    def get_DE_Pronoun(self, person, amount, Register):
        if Register == 0:
            for i in range(1, 6):
                if person == i:
                    if amount == 'Singular':
                        return self.DE_Pronoun[i-1][0][0].lower() + self.DE_Pronoun[i-1][0][1:]
                    if amount == 'Plural':
                        return self.DE_Pronoun[i-1][1][0].lower() + self.DE_Pronoun[i-1][1][1:]
        if Register == 1:
            for i in range(1, 6):
                if person == i:
                    if amount == 'Singular':
                        return self.DE_Pronoun[i - 1][0][0].upper() + self.DE_Pronoun[i - 1][0][1:]
                    if amount == 'Plural':
                        return self.DE_Pronoun[i - 1][1][0].upper() + self.DE_Pronoun[i - 1][1][1:]

    def get_EN_Pronoun(self, person, amount, voice, Register):
        if Register == 0:
            for i in range(1, 6):
                if person == i:
                    if amount == 'Singular':
                        return self.EN_Pronoun[i-1][0][0].lower() + self.EN_Pronoun[i-1][0][1:]
                    if amount == 'Plural':
                        return self.EN_Pronoun[i-1][1][0].lower() + self.EN_Pronoun[i-1][1][1:]
        if Register == 1:
            for i in range(1, 6):
                if person == i:
                    if amount == 'Singular':
                        return self.EN_Pronoun[i - 1][0][0].upper() + self.EN_Pronoun[i - 1][0][1:]
                    if amount == 'Plural':
                        return self.EN_Pronoun[i - 1][1][0].upper() + self.EN_Pronoun[i - 1][1][1:]

    def get_FR_Pronoun(self, person, amount, Register):
        if Register == 0:
            for i in range(1, 6):
                if person == i:
                    if amount == 'Singular':
                        return self.FR_Pronoun[i-1][0][0].lower() + self.FR_Pronoun[i-1][0][1:]
                    if amount == 'Plural':
                        return self.FR_Pronoun[i-1][1][0].lower() + self.FR_Pronoun[i-1][1][1:]
        if Register == 1:
            for i in range(1, 6):
                if person == i:
                    if amount == 'Singular':
                        return self.FR_Pronoun[i - 1][0][0].upper() + self.FR_Pronoun[i - 1][0][1:]
                    if amount == 'Plural':
                        return self.FR_Pronoun[i - 1][1][0].upper() + self.FR_Pronoun[i - 1][1][1:]

    def get_EN_Pronoun_Size(self):
        return len(self.EN_Pronoun)

    def get_EN_Noun_Size(self):
        return len(self.EN_Noun)

    def get_DE_Noun(self, index, Register):
        if Register == 0:
            return self.DE_Noun[index][0][0].lower() + self.DE_Noun[index][0][1:]
        if Register == 1:
            return self.DE_Noun[index][0][0].upper() + self.DE_Noun[index][0][1:]

    def get_EN_Noun(self, index, Register):
        if Register == 0:
            return self.EN_Noun[index][0][0].lower() + self.EN_Noun[index][0][1:]
        if Register == 1:
            return self.EN_Noun[index][0][0].upper() + self.EN_Noun[index][0][1:]

    def get_FR_Noun(self, index, Register):
        if Register == 0:
            return self.FR_Noun[index][0][0].lower() + self.FR_Noun[index][0][1:]
        if Register == 1:
            return self.FR_Noun[index][0][0].upper() + self.FR_Noun[index][0][1:]

    def get_state_DE_Noun(self, index):
        return self.DE_Noun[index][1]

    def get_state_EN_Noun(self, index):
        return self.EN_Noun[index][1]

    def show(self):
        print(self.EN_Noun)

    def filling(self):
        # DE
        Pronoun1 = open('Deutsch/DE_dictionary_pronoun.txt', mode='r')
        person = 0
        for line in Pronoun1:
            person += 1
            buf = line.split('-')
            if person == 1:
                self.add_DE_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 2:
                self.add_DE_Pronoun_Word([buf[0].strip() , buf[1].strip()])
            if person == 3:
                self.add_DE_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 4:
                self.add_DE_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 5:
                self.add_DE_Pronoun_Word([buf[0].strip(), buf[1].strip()])
        Pronoun1.close()


        Noun1 = open('Deutsch/DE_dictionary_noun.txt', mode='r')
        for line in Noun1:
            buf = line.split('-')
            if buf[1].strip() == 'Verbal':
                self.add_DE_Noun_Word(buf[0].strip(), buf[1].strip())
            elif buf[1].strip() == 'Object':
                self.add_DE_Noun_Word(buf[0].strip(), buf[1].strip())
        Noun1.close()

        Preposition1 = open('Deutsch/DE_dictionary_preposition.txt', mode='r')
        for line in Preposition1:
            self.add_DE_Preposition_Word(line.strip())
        Preposition1.close()

        # English
        Pronoun2 = open('English/EN_dictionary_pronoun.txt', mode='r')
        person = 0
        for line in Pronoun2:
            person += 1
            buf = line.split('-')
            if person == 1:
                self.add_EN_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 2:
                self.add_EN_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 3:
                self.add_EN_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 4:
                self.add_EN_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 5:
                self.add_EN_Pronoun_Word([buf[0].strip(), buf[1].strip()])
        Pronoun2.close()

        Noun2 = open('English/EN_dictionary_noun.txt', mode='r')
        for line in Noun2:
            buf = line.split('-')
            if buf[1].strip() == 'Verbal':
                self.add_EN_Noun_Word(buf[0].strip(), buf[1].strip())
            elif buf[1].strip() == 'Object':
                self.add_EN_Noun_Word(buf[0].strip(), buf[1].strip())
        Noun2.close()

        Preposition2 = open('English/EN_dictionary_Preposition.txt', mode='r')
        for line in Preposition2:
            self.add_EN_Preposition_Word(line.strip())
        Preposition2.close()

        # France
        Pronoun3 = open('France/FR_dictionary_pronoun.txt', mode='r')
        person = 0
        for line in Pronoun3:
            person += 1
            buf = line.split('-')
            if person == 1:
                self.add_FR_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 2:
                self.add_FR_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 3:
                self.add_FR_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 4:
                self.add_FR_Pronoun_Word([buf[0].strip(), buf[1].strip()])
            if person == 5:
                self.add_FR_Pronoun_Word([buf[0].strip(), buf[1].strip()])
        Pronoun3.close()

        Noun3 = open('France/FR_dictionary_noun.txt', mode='r')
        for line in Noun3:
            buf = line.split('-')
            if buf[1].strip() == 'Verbal':
                self.add_FR_Noun_Word(buf[0].strip(), buf[1].strip())
            elif buf[1].strip() == 'Object':
                self.add_FR_Noun_Word(buf[0].strip(), buf[1].strip())
        Noun3.close()

        Verbs3 = open('France/FR_dictionary_verb.txt', mode='r')
        for line in Verbs3:
            self.add_FR_Verbs_Word(line.strip())
        Verbs3.close()

        Preposition3 = open('France/FR_dictionary_Preposition.txt', mode='r')
        for line in Preposition3:
            self.add_FR_Preposition_Word(line.strip())
        Preposition3.close()


def check_register(word):
    if word[0].islower() == 1:
        return 0
    else:
        return 1


def del_suffix(text, s1):
    if text[-len(s1):-1]+text[-1] == s1:
        text2 = text[0:len(text)-len(s1)]
        return text2
    else:
        return text


def re_suffix(text):
    return text[0:-1]
